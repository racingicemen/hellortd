.. HelloRTD documentation master file, created by
   sphinx-quickstart on Wed Oct  3 06:55:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hello ReadTheDocs!
==================
A tutorial on how to write RTD tutorials. Each step of the process will be documented, so that someone
new to RTD can setup a tutorial with ease.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
